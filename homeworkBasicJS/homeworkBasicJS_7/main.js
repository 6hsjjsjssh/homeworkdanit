function displayElements(arr, element) {
    const list = document.getElementById(element);

    list.insertAdjacentHTML('afterbegin', `${
        arr.map(function (companyName) {
            return '<li>' + companyName + '</li>';
        }).join('')}`);
}

displayElements(["DELL", "ASUS", "HP", "Lenovo", "Apple"], 'companies-list')
