"use strict";

function createNewUser() {
    let _firstName = prompt('Type your first name, please: ');
    let _lastName = prompt('Type your last name, please: ');

        const  newUser = {
            get firstName() { return _firstName; },
            get lastName() { return _lastName; },
            getLogin: function () {
                return (this.firstName[0] + this.lastName).toLowerCase();
            }
        };

        Object.defineProperties(newUser , {

            'setFirstName': {
                set(value) {
                    _firstName = value;
                },
            },

            'setLastName': {
                set(value) {
                    _lastName = value;
                },
            },

        });

        return newUser;
}

const currentUser = createNewUser();
console.log(`После создания юзера: ${currentUser.firstName} ${currentUser.lastName} → ${currentUser.getLogin()}`)

currentUser.firstName = "test";
console.log(`Защита от перезаписи свойств напрямую: ${currentUser.firstName} ${currentUser.lastName} → ${currentUser.getLogin()}`)

// function setUserNames() {

//     Object.defineProperty(newUser, 'firstName', {
//       get: function() { return newUser.firstName },
//       set: function(newFirstName) { firstName = newFirstName; },
//       writable : true,
//       enumerable: true,
//       configurable: true
//     });

//     Object.defineProperty(newUser, 'lastName', {
//         get: function() { return lastName },
//         set: function(newLastName) { lastName = newLastName; },
//         writable : true,
//         enumerable: true,
//         configurable: true
//     });

// }
