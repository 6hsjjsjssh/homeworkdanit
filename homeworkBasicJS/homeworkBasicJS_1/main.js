let userName;

do {
    userName = prompt('Enter your name, please.', userName);
} while (!isNaN(userName) || userName.length <= 2)

let userAge;

do {
    userAge = +prompt('Enter your age, please.', userAge);
} while (!parseInt(userAge))

console.log('username: ', userName, ' typeof: ', typeof userName)

if (userAge < 18) {
    alert('You are not allowed to visit this website!')
} else if (userAge >= 18 && userAge <= 22) {
    if (confirm('Are you sure you want to continue?')) {
        alert('Welcome, ' + userName);
    } else {
        alert('You are not allowed to visit this website!')
    }
} else {
    alert('Welcome, ' + userName);
}
