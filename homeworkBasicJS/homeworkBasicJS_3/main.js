let someNumberN;
let someNumberM;
let mathOperation;
let allowedOperators = ['+', '-', '*', '/'];
let result;

function checkValues() {
    someNumberN = +prompt('Type first number, please: ', checkNumber(someNumberN) ? someNumberN : 0);
    someNumberM = +prompt('Type second number, please: ', checkNumber(someNumberM) ? someNumberM : 0);
    mathOperation = prompt('Select a math operation: "+", "-", "*", "/"', checkMathOperation(mathOperation) ? mathOperation : "");
}

function checkNumber(someNumber) {
    return !isNaN(someNumber) && typeof(someNumber) === "number"
}

function checkMathOperation () {
    let result;
    let mathOperation = allowedOperators;

    for(let i = 0; i <=3; i++){
        result = mathOperation.indexOf(allowedOperators[i]) > -1
        // if (mathOperation.indexOf(allowedOperators[i]) > -1) {
        //     result = true;
        // } else {
        //     result = false;
        // }
    }
    return result;
}

function calcOperation(numberN, numberM, selectedOperation) {
    let result;

    switch(selectedOperation){
        case "+":
            result = numberN + numberM;
            break;

        case "-":
            result = numberN - numberM;
            break;

        case "*":
            result = numberN * numberM;
            break;

        case "/":
            if(numberM === 0) {
                console.warn('Can not divide by 0!');
            }
            result = numberN / numberM;
            break;

        default:
            console.error('Error! Please, enter only one of the selected four operators: "+", "-", "*", "/"!');
    }
    return result;
}

do {
    checkValues();

    if (!checkNumber(someNumberN) || !checkNumber(someNumberM)) {
        alert('Error! Please, enter only number values!');
    } else if (!checkMathOperation(mathOperation)) {
        alert('Error! Please, enter correct operator!');
    }

} while (!checkNumber(someNumberN) || !checkNumber(someNumberM) || !checkMathOperation(mathOperation))

result = calcOperation(someNumberN, someNumberM, mathOperation);

console.log(`${someNumberN} ${mathOperation} ${someNumberM} = ${result}`);
