let someNumber;

do {
    someNumber = +prompt('Type an Integer number: ', someNumber);
} while (Number.isInteger(someNumber) == false)

for(let i = 0; i <= someNumber; i++){
    if (someNumber < 5){
        console.log(`Sorry, no numbers between 0 and ${someNumber}`);
        break;
    }
    if(i % 5 === 0){
        console.log(i);
    }
}
