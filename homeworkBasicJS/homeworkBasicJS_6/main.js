// const firstArr = ["string1", 50, "string2", 100, null, null, true, undefined, 150, 200, 1, null, "string3", true, {obj1: "xxx"}, 999999];
let secondArr = [];

function filterBy(arr, type) {
    console.log(`Исходный массив: `)
    console.log(arr);

    const filterElement = (i) => typeof i !== typeof type;
    let secondArr = arr.filter(filterElement);

    console.log(`После выполнения функции filterBy(): `)
    console.log(secondArr);
}

filterBy(["string1", 50, "string2", 100, null, null, true, undefined, 150, 200, 1, null, "string3", true, {obj1: "xxx"}, 999999, false], 0.01)
